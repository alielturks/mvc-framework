<?php

class App {
    
    public static $current_controller = "";
    public static $current_action = "index";
    public static $current_args = array();
    
    public function dispatch() {
        require(GLOBAL_PATH . 'app/controllers/' . App::$current_controller . '_controller.php');
        if(method_exists(App::$current_controller . '_controller', App::$current_action)) {
            $controller = App::$current_controller . '_controller';
            $action = App::$current_action;
            $controller = new $controller();
            $controller->view = $action;
            $controller->before_filter();
            $controller->$action(App::$current_args);
            require(GLOBAL_PATH . 'system/sys_view.php');
            sys_view::render($controller->layout, App::$current_controller, $controller->view, $controller->data);
            die();
        } else {
            die("Method '" . App::$current_action . "' not found for controller '" . App::$current_controller . "'");
        }
    }
    
    public function parse_url() {
        $url_data = explode("/", $_SERVER['REQUEST_URI']);
        if(isset($url_data[1]) && strlen($url_data[1]) > 0) {
            if(file_exists(GLOBAL_PATH . 'app/controllers/' . $url_data[1] . '_controller.php')) {
                App::$current_controller = $url_data[1]; 
                if(isset($url_data[2]) && strlen($url_data[2]) > 0) {
                    App::$current_action = $url_data[2];
                    if(count($url_data) > 2) {
                        for($i = 3; $i < count($url_data); $i++) {
                            if(strlen($url_data[$i]) > 0) {
                                App::$current_args[] = $url_data[$i];
                            }
                        }
                    }
                }
            } else {
                die("Controller '" . $url_data[1] . "_controller' not found");
            }
        } else {
            App::$current_controller = 'home';
        }
    }
    
    public function uses($controllers = array()) {
        
    }
}
?>