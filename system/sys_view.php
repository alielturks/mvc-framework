<?php

class sys_view {
    
    public function render($layout, $controller, $view, $data) {
        if(file_exists(GLOBAL_PATH . 'app/views/' . $controller . '/' . $view . '.php')) {
            ob_start();
            extract($data, EXTR_OVERWRITE);
            include(GLOBAL_PATH . 'app/views/' . $controller . '/' . $view . '.php');
            $action_render = ob_get_contents();
            ob_end_clean();
            if($layout == false) {
                echo $action_render;
                return;
            } else {
                if(file_exists(GLOBAL_PATH . 'app/views/layouts/' . $layout . '.php')) {
                    include(GLOBAL_PATH . 'app/views/layouts/' . $layout . '.php');
                } else {
                    die("Layout '" . $layout . "' not found");
                }
            }
        } else {
            die("View '" . $view . "' not found for '" . $controller . "_controller'");
        }
    }
    
}
?>
