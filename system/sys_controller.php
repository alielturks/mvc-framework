<?php

class sys_controller {
    public $view = "index";
    public $layout = "default";
    public $data = array();
    
    public function before_filter() {
        
    }
    
    public function after_filter() {
        
    }
    
    public function set($variables = array()) {
        $this->data = $variables;
    }
    
    public function render($view) {
        $this->view = $view;
    }
    
    public function redirect($url) {
        header('Location: ' . $url);
        die();
    }
    
    
}
?>
